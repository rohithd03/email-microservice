# To Install Dependencies ``` npm install ```
# To start the server ``` npm start ```
# API for POST request ``` http://localhost:3000/ ```
# Add a .env file which should contain
```
PORT - localhost port number
MAIL_ID - Your mail ID
MAIL_PASS - Your mail ID password
```

## POST request Route
```
The body should contain the json object
{
    "to": to mail address,
    "subject": mail subject,
    "email_body": email body content
}
On successful request, it sends email to the user.
```