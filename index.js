require('dotenv').config();
const express = require("express");
const nodemailer = require("nodemailer");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.MAIL_ID,
        pass: process.env.MAIL_PASS
    },
    tls: {
        rejectUnauthorized: false
    }
});

app.post("/", function(req, res){

    let message = {
        to: req.body.to,
        subject: req.body.subject,
        html: req.body.email_body
    };

    transporter.sendMail(message, (err, info) => {
        if (err) {
            console.log('Error occurred. ' + err);
            return res.send({"success": "false", "message": err});
        }
        
        return res.send({"success": "true", "message": "Email sent successfully"});
    });
});

const PORT = process.env.PORT;

app.listen(PORT || 3001, function(){
  console.log("Server started at port 3001");
});
